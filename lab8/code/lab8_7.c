#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void alarmHandler(int signum)
{
	printf("An alarm clock signal was received, signal code=%d\n",signum);
}
int main()
{
	signal(SIGALRM,alarmHandler); /* Install signal handler */
	alarm(3); /* Schedule an alarm in 3 seconds */
	printf("Waiting for signal ... \n");
	pause();   /* Wait for a signal */
	printf("Loop ends due to alarm signal \n");
}
/* Set flag to stop looping */
