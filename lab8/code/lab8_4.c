#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

void check_child_exit(int status){
    if (WIFEXITED(status))
        printf("Child ended normally. Exit code is %d\n",WEXITSTATUS(status));
    else if (WIFSIGNALED(status))
        printf("Child ended because of an uncaught signal, signal = %d\n",WTERMSIG(status));
    else if (WIFSTOPPED(status))
        printf("Child process has stopped, signal code = %d\n",WSTOPSIG(status));
    exit(EXIT_SUCCESS);
}

int main()
{
    int pid,status;
    char *argv[2];
    if ((pid=fork())==-1)    /*check for error*/
    {
        perror("fork");
        exit(1);
    }
    if (pid!=0)  		/*The parent process*/
    {
        printf("I am parent process %d\n",getpid());
        if (wait(&status)==-1) /*Wait for child*/
        {
            perror("wait");
        }
        check_child_exit(status);
    }
    else  			/*The child process*/
    {
        argv[0]="date";
        argv[1]=NULL;
        printf("I am child Process %d \n",getpid());
        printf("and i will replace myself by 'date'\n");
        execvp("date",argv); /*Execute date*/
        perror("execvp");
        exit(EXIT_FAILURE);
    }
}

