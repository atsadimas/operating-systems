#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
int main()
{
    int pid1,pid2;
    if ((pid1=fork())==-1) /* Check for error*/
    {
        perror("fork");
        exit(1);
    }
    if (pid1==0) /* First child */
        while(1) /* Infinite loop*/
        {
            printf("Process 1 is alive\n");
            sleep(1);
        }
    if ((pid2=fork())==-1) /* Check for error*/
    {
        perror("fork");
        exit(1);
    }
    if (pid2==0) /* Second child */
        while(1) /* Infinite loop*/
        {
            printf("Process 2 is alive\n");
            sleep(1);
        }
    sleep(2);
    kill(pid1,SIGSTOP); /* Suspend first child */
    sleep(5);
    kill(pid1,SIGCONT); /* Resume first child */
    sleep(2);
    kill(pid1,SIGINT); /* Interrupt first child */
    kill(pid2,SIGINT); /* Interrupt second child */
}
