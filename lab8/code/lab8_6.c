#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

// Define the function to be called when ctrl-c (SIGINT) signal is sent to process
void signalhandler(int signum)
{
	printf("Caught signal %d\n",signum);
	exit(signum);
}

int main()
{
	// Register signal and signal handler
	signal(SIGINT, signalhandler);

	while(1)
	{
		printf("Looping...\nWaiting for Ctrl-C...\n");
		sleep(1);
	}
	return EXIT_SUCCESS;
}

