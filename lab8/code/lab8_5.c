#include <stdio.h>
#include <unistd.h>

int main() {
    alarm(3); /*schedule an alarm signal in 3 seconds */
    printf("Looping forever ... \n");
    while(1);
    printf("This line should never be executed\n");
}
