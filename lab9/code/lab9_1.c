#include <stdio.h>
#include <pthread.h>
#define NUM_THREADS 4

void *thread_function( void *arg  )
{
    int id;
    id = *((int *)arg);
    printf( "Hello from thread %d!\n", id  );
    printf("Thread id = %lu \n",pthread_self());
    pthread_exit( NULL  );

}

int main(void)
{
    int i, tmp;
    int arg[NUM_THREADS] = {0,1,2,3};
    pthread_t thread[NUM_THREADS];
    pthread_attr_t attr;

    /* initialize and set the thread attributes */
    pthread_attr_init( &attr  );
    pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_JOINABLE  );

    /* creating threads */
    for ( i=0; i<NUM_THREADS; i++  )
    {
        tmp = pthread_create( &thread[i], &attr, thread_function,(void *)&arg[i] );
        if ( tmp != 0  )
        {
            fprintf(stderr, "Creating thread %d failed!",i);
            return 1;

        }
        else {
            fprintf(stdout, "Created Thread with id = %lu \n",thread[i]);
        }

    }

    /* joining threads */
    for ( i=0; i<NUM_THREADS; i++  )
    {
        tmp = pthread_join( thread[i], NULL  );
        if ( tmp != 0  )
        {
            fprintf(stderr,"Joining thread %d failed!",i);
            return 1;

        }

    }
    return 0;

}
