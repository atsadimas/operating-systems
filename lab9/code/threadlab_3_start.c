#include <stdio.h>
#include <pthread.h>

#define NTHREADS 10 //change number to 100,1000,10000,100000 (after that shows seg fault) 

void * thread_func(void * args);

int sum=0;
int
main()
{
  printf("pid of process:%d\n",getpid());
  pthread_t threads[NTHREADS];
  
  int count[NTHREADS];
  for (int i = 0; i < NTHREADS; ++i)
    {
      count[i]=i;
      pthread_create(&threads[i], NULL, thread_func, &count[i]);
       
    }
  //  sleep(10);
    printf ("Sum before exit is: %d\n",sum); //1 problem here due to not joining

  for (int i = 0; i < NTHREADS; ++i){
    pthread_join(threads[i],NULL);
  }
  printf ("Sum after exit is: %d\n",sum); //and another here--why???

}


void* thread_func (void *args)
{
  
  int *argptr = args;
  sum=sum+1;
  /* Print the local copy of the argument */
  //printf ("Argument is %d\n", *argptr);
  pthread_exit (NULL);
}

