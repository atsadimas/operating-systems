#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


//A thread is spawned by defining a function and it's arguments which will be processed in the thread
void* print_message_function( void *ptr );

//A void* is a pointer to any pointer type
main()
{
     pthread_t thread1, thread2;
     int var1=1;
     int var2=2;

     int  iret1, iret2;
    /* Create independent threads each of which will execute function */

     iret1 = pthread_create( &thread1, NULL, print_message_function, &var1);
     iret2 = pthread_create( &thread2, NULL, print_message_function, &var2);
     //sleep(3);
     printf("Main print before join\n");

     /* Wait till threads are complete before main continues. Unless we  */
     /* wait we run the risk of executing an exit which will terminate   */
     /* the process and all threads before the threads have completed.   */
     
     //pthread_join(thread1, NULL);
     //pthread_join(thread2, NULL); 

     printf("Thread 1 returns: %d\n",iret1);
     printf("Thread 2 returns: %d\n",iret2);
     //exit(0);
     pthread_exit(0);
}

void* print_message_function( void *ptr )
{
     
     int* intvar=ptr;
     printf("This is thread argument: %d \n", *intvar);
     
}
