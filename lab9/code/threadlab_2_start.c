#include <stdio.h>
#include <pthread.h>

#define NTHREADS 10

void * thread_func(void * args);

int
main()
{
  printf("pid of process:%d\n",getpid());
  pthread_t threads[NTHREADS];
  
  int count[NTHREADS];
  for (int i = 0; i < NTHREADS; ++i)
    {
      count[i]=i;
      pthread_create(&threads[i], NULL, thread_func, &count[i]);
       
    }
    sleep(10);
  printf ("After thread creation...\n");

  
  for (int i = 0; i < NTHREADS; ++i){
    pthread_join(threads[i],NULL);
  }
}


void* thread_func (void *args)
{
  
  int *argptr = args;
  
  /* Print the local copy of the argument */
  printf ("Argument is %d\n", *argptr);
  pthread_exit (NULL);
}

