#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>

int main()
{
    long result;
    int  fd[2];
    ;
    if (pipe(fd) < 0)
        perror("creat() error");
    else
    {
        errno = 0;
        puts("examining PIPE_BUF limit");
        if ((result=fpathconf(fd[0], _PC_PIPE_BUF))==-1)
            if (errno == 0)
                puts("There is no limit to PIPE_BUF.");
            else perror("fpathconf() error");
        else
            printf("PIPE_BUF is %ld\n", result);
        close(fd[0]);
        close(fd[1]);


    }

}
