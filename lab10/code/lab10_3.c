#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    char *path = "/bin/ls";
    char *arg0 = "ls";
    pid_t pid;
    int fd;
    int status;
    pid = fork();
    if (pid == 0) {
        fd = open("temp111", O_WRONLY|O_CREAT|O_TRUNC, 0755);
        dup2(fd, STDOUT_FILENO);
        close(fd);
        if (execl(path, arg0, NULL) == -1)
            perror("execl");

    } else {
        close(fd);
        wait(&status);

    }

}
