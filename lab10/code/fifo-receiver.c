#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define MSGSIZE 65
char *fifo="myfifo";

int main(int argc, char *argv[]){

	int fd, i, nwrite;
	char msgbuf[MSGSIZE+1];

	if (argc>2) {
		printf("Usage: %s & \n", argv[0]);
		exit(1);
	}

	if (mkfifo(fifo,0666) == -1) {
		if (errno!=EEXIST) {
			perror("receiver: mkfifo");
			exit(5);
		}
	}

	if ((fd=open(fifo,O_RDWR)) < 0) {
		perror("fifo open error");
		exit(6);
	}

	for (;;) {
		if (read(fd,msgbuf,MSGSIZE+1) < 0) {
			perror("reading error");
			exit(7);
		}
		printf("\nMessage received: %s\n",msgbuf);
		fflush(stdout);
	}
}
