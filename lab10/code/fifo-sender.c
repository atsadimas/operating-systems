#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define MSGSIZE 65
char *fifo="myfifo";

int main(int argc, char *argv[]) {
	int fd, i, nwrite;
	char msgbuf[MSGSIZE+1];

	if (argc>2) {
		printf("Usage: %s message \n", argv[0]);
		exit(1);
	}

	if ((fd=open(fifo,O_WRONLY| O_NONBLOCK)) < 0) {
		perror("fifo open error");
		exit(6);
	}

	for (i=1; i<argc; i++) {
		if (strlen(argv[i]) > MSGSIZE) {
			printf("Message with prefix %.*s Too long - Ignored\n",10,argv[i]);
			fflush(stdout);
			continue;
		}
		strcpy(msgbuf, argv[i]);
		if ((nwrite=write(fd,msgbuf,MSGSIZE+1)) == -1) {
			perror("write error");
			exit(5);
		}
	}
	exit(0);

}
