#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#define READ 0 /* Read end of pipe */
#define WRITE 1 /* Write end of pipe */
char *phrase="This is a test phrase";

int main()
{
    int pid, fd[2], df[2], bytes;
    char message[100];
    if (pipe(fd)==-1)     /*create a pipe*/
    {
        perror("pipe");
        exit(1);
    }
    if ((pid=fork())==-1)    /*fork a child*/
    {
        perror("fork");
        exit(1);

    }
    if (pid==0)     /*child writer */
    {
        close(fd[READ]);/*close unused end */
        write(fd[WRITE], phrase, strlen(phrase)+1);
        close(fd[WRITE]);

    }/*close used end */
    else        /* parent reader */
    {
        close(fd[WRITE]);/*close unused end */
        bytes=read(fd[READ], message, sizeof(message));
        printf("Read %d bytes : %s\n", bytes, message);
        close(fd[READ]);

    } /*close used end */

}
