#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{
	int file=0;
	if((file=open("t",O_RDONLY)) < -1)
		exit(1);

	char buffer[5];
	if(read(file,buffer,5) != 5)  return 1;
	buffer[5]='\0';
	printf("%s\n",buffer);

	if(lseek(file,10,SEEK_SET) < 0) return 1;

	if(read(file,buffer,5) != 5)  return 1;
	buffer[5]='\0';
	printf("%s\n",buffer);

	return 0;

}
