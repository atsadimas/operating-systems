#include <stdio.h>
#include <sys/file.h>
#include <fcntl.h> /* Needed for Solaris */
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int fd, bytes;
	char buf[50];

	if ((fd=open("t", O_WRONLY | O_CREAT, 0600))==-1)
	{
		perror("open");
		exit(1);

	}
	bytes=write(fd, "first write. ", 13);
	printf("%d bytes were written \n", bytes);
	close(fd);

	if ((fd=open("t", O_WRONLY | O_APPEND))==-1)
	{
		perror("open");
		exit(1);

	}
	bytes=write(fd, "second write. \n", 14);
	printf("%d bytes were written \n", bytes);
	close(fd);

	if ((fd=open("t", O_RDONLY ))==-1)
	{
		perror("open");
		exit(1);

	}
	bytes=read(fd, buf, sizeof(buf));
	printf("%d bytes were read \n", bytes);
	close(fd);

	buf[bytes]='\0';
	printf("%s", buf);

}
