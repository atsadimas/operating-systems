#include <signal.h>
#include <unistd.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

int count=0;
void alarmhandler(int);

int main(){
    int p[2];
    int pipe_size=0;
    char c='a';

    signal(SIGALRM,alarmhandler);

    if (pipe(p) == -1) {
        perror("pipe");
        exit(1);
    }

    pipe_size=fpathconf(p[0],_PC_PIPE_BUF);

    printf("Maximun size of (atomic) write to pipe: %d bytes\n",pipe_size);
    printf("The respective POSIX value %d",_PC_PIPE_BUF);

    while(1) {
        alarm(10);
        write(p[1],&c,1);
        alarm(0);
        if (++count % 4096 == 0) {
            printf("%d characters in pipe\n",count);
        }
    }
}

void alarmhandler(int signo){
    printf("write blocked after %d characters \n", count);
    exit(0);
}
