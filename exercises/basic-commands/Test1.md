# sort

Αντιγράψτε το αρχείο στον κατάλογό σας

wget https://gitlab.com/atsadimas/operating-systems/-/raw/master/exercises/basic-commands/greek-population.txt -O greek-population.txt

ή

curl https://gitlab.com/atsadimas/operating-systems/-/raw/master/exercises/basic-commands/greek-population.txt --output greek-population.txt


Το αρχείο αυτό περιέχει τον πληθυσμό της Ελλάδας τα τελευταία 40 χρόνια.
Μελετήστε την εντολή [sort](https://www.tecmint.com/sort-command-linux/) και προσπαθήστε να απαντήσετε στις παρακάτω ερωτήσεις:

* Ταξινομήστε τα δεδομένα ώστε να φαίνονται πρώτα οι χρονιές με το μεγαλύτερο πληθυσμό
* Ταξινομήστε τα δεδομένα ώστε να φαίνονται πρώτα οι χρονιές με το μικρότερο πληθυσμό
* Εμφανίστε τις 5 χρονιές (μαζί με τα υπόλοιπα στοιχεία) που η Ελλάδα είχε το μεγαλύτερο πληθυσμό, ταξινομημένες με φθίνουσα διάταξη
* Εμφανίστε τις 3 χρονιές (μαζί με τα υπόλοιπα στοιχεία) που η Ελλάδα είχε το μικρότερο πληθυσμό, ταξινομημένες με αύξουσα διάταξη

# grep

Αντιγράψτε το αρχείο στον κατάλογό σας. Στο αρχείο αυτό υπάρχουν 3 στήλες: hostname, ip-address, status code


wget https://gitlab.com/atsadimas/operating-systems/-/raw/master/exercises/basic-commands/ip-addresses.txt -O ip-addresses.txt

ή

curl https://gitlab.com/atsadimas/operating-systems/-/raw/master/exercises/basic-commands/ip-addresses.txt --output ip-addresses.txt


* Εμφανίστε μόνο τις ip διευθύνσεις
* Εμφανίστε μόνο τα hostnames
* Εμφανίστε τις γραμμές που δεν έχουν status code 500


# find 

* βρείτε τα C αρχεία (κατάληξη .c) που υπάρχουν στον κατάλογο /usr/include
* μετρήστε πόσα header files (κατάληξη .h) υπάρχουν στον κατάλογο /usr/include
* βρείτε πόσοι κατάλογοι με το όνομα src βρίσκονται κάτω από τον /usr/include

# Ανακατευθύνσεις

* Αποθηκεύστε σε αρχεία τα αποτελέσματα της προηγούμενης άσκησης με την find. Δηλαδή δημιουργήστε τα αρχεία c-files.txt, h-files.txt και src-directories.txt 

Υλικό

* Σημειώσεις του μαθήματος
* το βιβλίο [The Linux Command Line](https://linuxcommand.org/tlcl.php) 