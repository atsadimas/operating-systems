#!/bin/bash - 
#===============================================================================
#
#          FILE: params.sh
# 
#         USAGE: ./params.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: ANARGYROS TSADIMAS (), tsadimas@gmail.com
#  ORGANIZATION: Harokopio University
#       CREATED: 11/13/2017 04:05:25 PM
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

for i in "$*"
do
    echo "-$i-"
done

for j in "$@"
do
    echo "-$j-"
done
