#!/bin/bash
# Check the given file exists #
if [ ! -f $3 ]
then
	echo "Filename given \"$3\" doesn't exist"
	exit
fi

case "$1" in
	# Count number of lines matches
	-l)
	echo -n "Number of lines matches with the pattern $2 : "
	grep -c -i $2 $3
	;;
	# Count number of words matches
	-w)
	echo -n "Number of words matches with the pattern $2 : "
	if [[ $(uname) == "Linux" ]]
	then
		grep -o -i $2 $3 | wc -l
	elif [[ $(uname) == "SunOS" ]]
	then
		ggrep -o -i $2 $3 | wc -l
	else
		echo "$(uname) is not supported"
	fi
	;;
	# print all the matched lines
	-p)
	echo "Lines matches with the pattern $2 : "
	grep -i $2 $3
	;;
	# Delete all the lines matches with the pattern
	-d)
	echo "After deleting the lines matches with the pattern $2 : "
	sed -n "/$2/!p" $3
	;;
*) echo "Invalid option"
	;;
	esac

