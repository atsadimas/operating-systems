#!/bin/bash - 
#===============================================================================
#
#          FILE: ifs.sh
# 
#         USAGE: ./ifs.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: ANARGYROS TSADIMAS (), tsadimas@gmail.com
#  ORGANIZATION: Harokopio University
#       CREATED: 11/13/2017 03:35:38 PM
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

IFS.OLD=$IFS
IFS=$'\n'
for entry in $(cat /etc/passwd)
do
    echo "Values in $entry - "
    IFS=:
    for value in $entry
    do
        echo "    $value"
    done
done
