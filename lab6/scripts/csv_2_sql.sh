#!/bin/bash - 
#===============================================================================
#
#          FILE: csv_2_sql.sh
# 
#         USAGE: ./csv_2_sql.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: ANARGYROS TSADIMAS (), tsadimas@gmail.com
#  ORGANIZATION: Harokopio University
#       CREATED: 11/13/2017 05:34:17 PM
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


outfile='insert_script.sql'
IFS=','
while read lname fname city
do
    cat >> $outfile << EOF
    INSERT INTO  people (lname, fname, city) VALUES ('$lname', '$fname', '$city');
EOF
done < ${1}

